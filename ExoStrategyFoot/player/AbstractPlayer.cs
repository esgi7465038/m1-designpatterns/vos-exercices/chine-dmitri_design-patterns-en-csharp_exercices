namespace ExoStrategyFoot;

public abstract class AbstractPlayer
{
    private int numero;
    private IPlayingAction[]? playingAction;

    protected AbstractPlayer(int numero, IPlayingAction[] playingAction)
    {
        this.numero = numero;
        this.playingAction = playingAction;
    }

    public int Numero
    {
        get { return numero; }
    }


    public void DoAction(int idAction)
    {
        if (idAction >= 0 && idAction < this.playingAction.Length)
        {
            Console.WriteLine("Action de numero " + this.Numero + " : " + this.playingAction[idAction].ActionToDo());
        }
    }
}