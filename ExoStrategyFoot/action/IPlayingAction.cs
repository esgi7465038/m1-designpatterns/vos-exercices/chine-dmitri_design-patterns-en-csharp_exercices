namespace ExoStrategyFoot;

public interface IPlayingAction
{
    public string ActionToDo();
}