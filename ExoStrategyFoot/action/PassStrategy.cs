namespace ExoStrategyFoot;

public class PassStrategy : IPlayingAction
{
    public string ActionToDo()
    {
        return "Joueur à passer le ballon";
    }
}