﻿// See https://aka.ms/new-console-template for more information

using ExoStrategyFoot;

Console.WriteLine("Jeux commence!");

var ailier = new Ailier(10,
    new IPlayingAction[] { new TirStrategy() });
var gadien = new Gardien(9,
    new IPlayingAction[] { new PassStrategy() });
var milleu = new Milleu(7,
    new IPlayingAction[]
    {
        new TirStrategy(),
        new PassStrategy()
    });
var defenseur = new Defenseur(5,
    new IPlayingAction[]
    {
        new TirStrategy(),
        new PassStrategy(),
        new DegagementStrategy()
    });

ailier.DoAction(0);
milleu.DoAction(1);
defenseur.DoAction(2);
gadien.DoAction(0);