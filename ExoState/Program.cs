﻿using ExoState.exeption;
using ExoState;

Etudiant UnEtudiant = new Etudiant("Pal");
string input = "";

while (true)
{
    Console.WriteLine("Entrez une action : \\n1 - DireBonjour, \\n2 - PartirEnVacance, \\n3 - RentrerDeVacance, \\n4 - Travailler");
    input = Console.ReadLine();

    try
    {
        switch (input)
        {
            case "1":
                Console.WriteLine(UnEtudiant.DireBonjour());
                break;
            case "2":
                UnEtudiant.PartirEnVacance();
                break;
            case "3":
                UnEtudiant.RentrerDeVacance();
                break;
            case "4":
                UnEtudiant.Travailler();
                break;
            default:
                Console.WriteLine("Action non reconnue.");
                break;
        }
    }
    catch (TransitionImpossibleException ex)
    {
        Console.WriteLine("Exception Transition Impossible: " + ex.Message);
    }
}