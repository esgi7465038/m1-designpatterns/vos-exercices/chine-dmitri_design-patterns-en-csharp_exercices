using System.Transactions;
using ExoState.exeption;

namespace ExoState.etat;

public class EtatHeureux : EtatAbstract
{
    public void Travailler(Etudiant contexte)
    {
        if (contexte.EtatCourant == contexte.LesEtats[Etudiant.etatHeureux])
            throw new TransitionImpossibleException(
                "Je suis en vacances, je ne veux pas et je ne peux pas travailler!");
    }
}