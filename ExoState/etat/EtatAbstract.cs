namespace ExoState.etat;

public abstract class EtatAbstract : IEtatEtudiant
{
    public string DireBonjour(Etudiant contexte)
    {
        return "Hellooo!! I'am happy!";
    }

    //  Certain ne font rien dans cet état concret.
    //  Dans un autre état, ce comportement pourra 
    //  effectuer un traitement et / ou provoquer une transition
    public void PartirEnVacance(Etudiant contexte)
    {
        Console.WriteLine("Je part en vacanses !");
    }

    //  Ce comportement effectue un traitement et provoque une transition
    public void RentrerDeVacance(Etudiant contexte)
    {
        Console.WriteLine("J'étais Heureux... Maintenant je suis Contrarié!");
        contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
    }

    public void Travailler(bool estTravaille)
    {
        if (estTravaille)
        {
            Console.WriteLine("Je vais travailler!");
        }
        else
        {
            Console.WriteLine("J'ai terminé travailler!");
        }
    }
}