namespace ExoState.etat;

public interface IEtatEtudiant
{
    string DireBonjour(Etudiant contexte);
    void PartirEnVacance(Etudiant contexte);
    void RentrerDeVacance(Etudiant contexte);
    void Travailler(bool estTravaille);
}