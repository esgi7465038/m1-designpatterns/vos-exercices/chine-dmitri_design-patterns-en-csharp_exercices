﻿// See https://aka.ms/new-console-template for more information

using ExempleVanneCustom;

Vanne? MaVanne;
MaVanne = new Vanne(200);

Console.Write("Ouvrir MaVanne: ");
MaVanne.Position = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("MaVanne est ouverte à {0}", MaVanne.Position);
Console.WriteLine("\nDescription de MaVanne: {0}", MaVanne);

if (MaVanne.Position > 75)
    MaVanne.Ouvrir(75);
Console.WriteLine("Débit: " + MaVanne.Debit);

VanneRegulee vanneRegulee = new VanneRegulee(true);
Console.WriteLine("Before regulation, VanneRegulee is: " + vanneRegulee.ToString());

vanneRegulee.IsReguleOn = true;
vanneRegulee.Regule(50);
Console.WriteLine("After regulation, VanneRegulee is: " + vanneRegulee.ToString());

if (MaVanne.Position > 75)
    MaVanne.Ouvrir(75);
Console.WriteLine("Débit: " + MaVanne.Debit);