namespace ExempleVanneCustom;

public class VanneRegulee : Vanne, IRegulable
{
    private bool isReguleOn;
    
    public VanneRegulee(bool isReguleOn) : base()
    {
        this.isReguleOn = isReguleOn;
    }

    public bool IsReguleOn
    {
        get { return isReguleOn; }
        set { isReguleOn = value; }
    }

    public void Regule(int consigneDebit)
    {
        if (consigneDebit < debitMax)
        {
            Position = consigneDebit * 100 / debitMax;
        }
    }
    
    public override string ToString()
    {
        string baseToString = base.ToString();
        return $"{baseToString}, isReguleOn: {this.isReguleOn}";
    }
}