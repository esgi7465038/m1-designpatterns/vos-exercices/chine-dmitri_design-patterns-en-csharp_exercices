namespace ExempleVanneCustom;

public interface IRegulable
{
    void Regule(int debitConsigne);
    bool IsReguleOn { get; set; }
}