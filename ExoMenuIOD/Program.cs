﻿// See https://aka.ms/new-console-template for more information

using ExoMenuIOD.menu;

Console.WriteLine("***************************************");
Console.WriteLine("* Menu avec injection de dépendances. *");
Console.WriteLine("***************************************");

//  Création des plats
Entree entree = new("Salade verte");
Entree entree2 = new("Charcuterie");
PlatPrincipal platPrincipal = new("Pizza");
PlatPrincipal platPrincipal2 = new("Risotto");
Dessert dessert = new("Tiramisu");


//  Création du menu
Menu menu1 = new Menu(1);

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(entree2).AddPlat(platPrincipal2).AddPlat(dessert);
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] nouveauxPlats = { new PlatPrincipal("Burger"), 
    new PlatPrincipal("Pasta Carbonara"),
    new Dessert("Gâteau au chocolat"),
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu menu2 = new Menu(2, nouveauxPlats);
Console.WriteLine(menu2.Consommer());

