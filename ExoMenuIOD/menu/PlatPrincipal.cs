namespace ExoMenuIOD.menu;

public class PlatPrincipal : IPlat
{
    public string Nom { get; }

    public PlatPrincipal(string nom)
    {
        Nom = nom;
    }

    public string Manger() 
    {
        return "=> Je mange plat principal " + Nom + "\n";
    }
}