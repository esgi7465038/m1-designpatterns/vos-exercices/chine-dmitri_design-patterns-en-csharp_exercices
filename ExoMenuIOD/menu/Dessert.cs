namespace ExoMenuIOD.menu;

public class Dessert: IPlat
{
    public string Nom { get; }

    public Dessert(string nom)
    {
        Nom = nom;
    }
    
    public string Manger() 
    {
        return "=> Je mange dessert " + Nom + "\n";
    }
}