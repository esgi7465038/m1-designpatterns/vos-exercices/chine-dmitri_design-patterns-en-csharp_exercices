﻿// See https://aka.ms/new-console-template for more information
using ConsoleAppExercice1;

Feutre unFeutreBleu;
Feutre unAutreFeutreBleu = new("Plastique", ConsoleColor.Blue);
Feutre unFeutreRouge;

Console.WriteLine("-----> 1");
unFeutreBleu = new Feutre();
unFeutreBleu.Debouche();
unFeutreBleu.Ecrire("Hello les bleus");

Console.WriteLine("-----> 2");
unAutreFeutreBleu.Debouche();
unAutreFeutreBleu.Ecrire("Encore en bleu...");
unAutreFeutreBleu.Bouche();
unAutreFeutreBleu.Ecrire("Tentative bouché...");

Console.WriteLine("-----> 3");
unFeutreRouge = new Feutre("Métal", ConsoleColor.Red, false);
unFeutreRouge.Debouche();
unFeutreRouge.Ecrire("Cette fois c'est en rouge");
unFeutreRouge.Bouche();
unAutreFeutreBleu.Ecrire("Tentative bouché...");

Console.Write("\n\n\t\tUNE TOUCHE POUR QUITTER AVEC CODE 0");
Console.ReadKey();