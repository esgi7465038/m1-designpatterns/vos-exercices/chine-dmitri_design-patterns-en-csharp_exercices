namespace Menu;

public abstract  class AbstractEntree : IPlat
{
    public string Nom { get; set; }
    public double Prix { get; set; }
    public int Calories { get; set; }
    public string Description { get; set; }
    public string Image { get; set; }

    public string Cuisiner()
    {
        return "Cuisiner une entrée";
    }

    public string Preparer()
    {
        return "Préparer une entrée";
    }

    public string Servir()
    {
        return "Servir une entrée";
    }

    public string Manger()
    {
        return "Je mange Entrée";
    }

    public string ToString()
    {
        return "Entrée";
    }
}