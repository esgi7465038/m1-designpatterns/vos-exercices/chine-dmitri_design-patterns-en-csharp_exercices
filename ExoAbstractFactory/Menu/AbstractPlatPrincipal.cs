namespace Menu;

public abstract class AbstractPlatPrincipal : IPlat
{
    public string Nom { get; set; }
    public double Prix { get; set; }
    public int Calories { get; set; }
    public string Description { get; set; }
    public string Image { get; set; }

    public string Cuisiner()
    {
        return "Cuisiner un plat principal";
    }

    public string Preparer()
    {
        return "Préparer un plat principal";
    }

    public string Servir()
    {
        return "Servir un plat principal";
    }

    public string Manger()
    {
        return "Je mange Plat principal";
    }

    public string ToString()
    {
        return "Plat principal";
    }
}