﻿using Menu;

namespace ExoAbstractFactory.Menu.PlatsPrincipaux
{
    internal class PlatPrincipalEnfant : AbstractPlatPrincipal
    {
        public PlatPrincipalEnfant(string nom)
        {
            base.Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le plat principal enfant " + Nom + "\n";
        }
    }
}