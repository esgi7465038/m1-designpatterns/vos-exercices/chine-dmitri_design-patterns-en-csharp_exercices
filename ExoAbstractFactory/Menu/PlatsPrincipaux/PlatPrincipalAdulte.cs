﻿using Menu;

namespace ExoAbstractFactory.Menu.PlatsPrincipaux
{
    internal class PlatPrincipalAdulte : AbstractPlatPrincipal
    {
        public PlatPrincipalAdulte(string nom)
        {
            base.Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le plat principal adulte " + Nom + "\n";
        }
    }
}