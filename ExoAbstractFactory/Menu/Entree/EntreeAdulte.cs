﻿using Menu;

namespace ExoAbstractFactory.Menu.Entree
{
    internal class EntreeAdulte : AbstractPlatPrincipal
    {
        public EntreeAdulte(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange l'entrée " + Nom + "\n";
        }
    }
}