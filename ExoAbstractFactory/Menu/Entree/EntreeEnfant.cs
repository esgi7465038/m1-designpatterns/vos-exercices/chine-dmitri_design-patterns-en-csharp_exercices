﻿using Menu;

namespace ExoAbstractFactory.Menu.Entree
{
    internal class EntreeEnfant : AbstractPlatPrincipal
    {
        public EntreeEnfant(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange l'entrée " + Nom + "\n";
        }
    }
}