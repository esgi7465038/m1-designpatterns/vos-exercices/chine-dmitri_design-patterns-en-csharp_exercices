namespace Menu;

public abstract class AbstractDessert : IPlat
{
    public string Nom { get; set; }
    public double Prix { get; set; }
    public int Calories { get; set; }
    public string Description { get; set; }
    public string Image { get; set; }

    public string Cuisiner()
    {
        return "Cuisiner un dessert";
    }

    public string Preparer()
    {
        return "Préparer un dessert";
    }

    public string Servir()
    {
        return "Servir un dessert";
    }

    public string Manger()
    {
        return "Je mange Dessert";
    }

    public string ToString()
    {
        return "Dessert";
    }
}