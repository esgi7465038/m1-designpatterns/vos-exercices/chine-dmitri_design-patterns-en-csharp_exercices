﻿using Menu;

namespace ExoAbstractFactory.Menu.Dessert
{
    internal class DessertEnfant : AbstractDessert
    {
        public DessertEnfant(string nom)
        {
            base.Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le dessert enfant " + base.Nom + "\n";
        }
    }
}
