﻿using Menu;

namespace ExoAbstractFactory.Menu.Dessert
{
    internal class DessertAdulte : AbstractDessert
    {
        public DessertAdulte(string nom)
        {
            base.Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le dessert adult " + base.Nom + "\n";
        }
    }
}