﻿using Factory;
using Menu;

Console.WriteLine("*************************");
Console.WriteLine("* Menu avec PlatFactory *");
Console.WriteLine("*************************");

//  Création de la fabrique
PlatEnfantFactory enfantFactory = new PlatEnfantFactory();

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entree = enfantFactory.CreateEntree("Salade verte");
PlatPrincipal platPrincipal = (PlatPrincipal)enfantFactory.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(enfantFactory.CreateDessert("Tiramizu"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { enfantFactory.CreateEntree("Nems"),
    enfantFactory.CreateEntree("Giozza"),
    enfantFactory.CreatePlatPrincipal("Bobun"),
    enfantFactory.CreateDessert("Moshi"),
    enfantFactory.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());

