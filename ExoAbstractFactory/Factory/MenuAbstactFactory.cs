namespace Factory;

public class MenuAbstactFactory
{
    public AbstractPlatFactory CreateFactory(string type)
    {
        switch (type)
        {
            case "Enfant":
                return new PlatEnfantFactory();
            case "Adulte":
                return new PlatAdultFactory();
            default:
                throw new Exception("Factory n'existe pas!");
        }
    }
}