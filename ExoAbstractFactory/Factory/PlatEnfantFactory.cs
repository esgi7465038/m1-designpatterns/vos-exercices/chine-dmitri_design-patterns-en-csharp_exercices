﻿using ExoAbstractFactory.Menu.Dessert;
using ExoAbstractFactory.Menu.PlatsPrincipaux;
using Menu;

namespace Factory
{
    internal class PlatEnfantFactory : AbstractPlatFactory
    {
        public IPlat CreateEntree(string nom)
        {
            return new PlatPrincipalEnfant(nom);
        }

        public IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalEnfant(nom);
        }

        public IPlat CreateDessert(string nom)
        {
            return new DessertEnfant(nom);
        }
    }
}