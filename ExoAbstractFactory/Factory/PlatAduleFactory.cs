﻿using ExoAbstractFactory.Menu.Dessert;
using ExoAbstractFactory.Menu.PlatsPrincipaux;
using Menu;

namespace Factory
{
    internal class PlatAdultFactory : AbstractPlatFactory
    {
        public IPlat CreateEntree(string nom)
        {
            return new PlatPrincipalAdulte(nom);
        }

        public IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalAdulte(nom);
        }

        public IPlat CreateDessert(string nom)
        {
            return new DessertAdulte(nom);
        }
    }
}