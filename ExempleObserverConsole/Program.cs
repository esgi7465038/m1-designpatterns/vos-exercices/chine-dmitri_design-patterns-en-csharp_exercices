﻿// See https://aka.ms/new-console-template for more information

using ExempleObserverConsole;

Console.WriteLine("Instanciation des observateurs...");
FirstObservable obs1 = new(ConsoleColor.Green, "OBS-1");
FirstObservable obs2 = new(ConsoleColor.Red, "OBS-2");
ObservateurAlerte alertObserver = new ObservateurAlerte("Alert Observer");


Console.WriteLine("-- Instanciation du sujet concret...");
MainSujet sujet = MainSujet.Instance;
Console.WriteLine("-- Instanciation du sujetTest concret...");
MainSujet sujetTest = MainSujet.Instance;


if (ReferenceEquals(sujet, sujetTest))
{
    Console.WriteLine("MainSujet est un singleton!");
}
else
{
    Console.WriteLine("MainSujet n'est pas un singleton!");
}

Console.WriteLine("-- Modification consigne du sujet...");
sujet.Consigne = 15;
Thread.Sleep(1000);

Console.WriteLine("-- Inscription ALLERT: ");
sujet.AjouteObservateur(alertObserver);
Console.WriteLine("     " + sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(2000);

Console.WriteLine("Inscription obs1: ");
sujet.AjouteObservateur(obs1);
Console.WriteLine("     " + sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(2000);

Console.WriteLine("Inscription obs2: ");
sujet.AjouteObservateur(obs2);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(2000);

Console.WriteLine("Modification consigne du sujet");
sujet.Consigne = 30;
Thread.Sleep(10000);

Console.WriteLine("Retrait  obs1: ");
sujet.RetireObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

sujet.Stop = true;
sujet.LaTache.Join();

Console.WriteLine("Sujet arrété");
sujet.RetireObservateur(obs1);
sujet.RetireObservateur(obs2);
sujet.RetireObservateur(alertObserver);