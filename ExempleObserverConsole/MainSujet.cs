namespace ExempleObserverConsole;

using observable;

public class MainSujet : ISujet
{
    private static MainSujet instance = null;
    private static readonly object padlock = new object();
    private readonly List<IObservable> Observateurs = new List<IObservable>();
    private int consigne = 0;

    public readonly Thread LaTache;
    public int NbObservateur => this.Observateurs.Count;
    public int ValObservee { get; private set; } = 0;
    public bool Stop { get; set; }


    private MainSujet()
    {
        LaTache = new(Simulation);
        LaTache.Start();
    }

    public static MainSujet Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new MainSujet();
                }

                return instance;
            }
        }
    }

    public void AjouteObservateur(IObservable observateur)
    {
        Observateurs.Add(observateur);
    }

    public void RetireObservateur(IObservable observateur)
    {
        Observateurs.Remove(observateur);
    }

    public void NotifierObservateurs()
    {
        foreach (IObservable observateur in Observateurs)
        {
            observateur.Actualiser(this);
        }
    }

    public int Consigne
    {
        get { return consigne; }
        set
        {
            if (value < 0)
                consigne = 0;
            else if (value > 100)
                consigne = 100;
            else
                consigne = value;
        }
    }

    private void Simulation()
    {
        while (!Stop)
        {
            if (ValObservee != consigne)
            {
                //  Mise à jour de la valeur observée
                if (ValObservee < consigne)
                    ValObservee++;
                else
                    ValObservee--;

                //  On prévient tous les observateurs
                this.NotifierObservateurs();
            }

            Thread.Sleep(1000);
        }
    }

    public int getVal()
    {
        return this.ValObservee;
    }
}