namespace ExempleObserverConsole;

using observable;

public class FirstObservable : IObservable
{
    private ConsoleColor ForegroundColor { get; set; }

    public string Name { get; }

    public FirstObservable(ConsoleColor foregroundColor, string name)
    {
        this.Name = name;
        ForegroundColor = foregroundColor;
    }

    public void Actualiser(ISujet sujet)
    {
        ConsoleColor old = Console.ForegroundColor;
        Console.ForegroundColor = ForegroundColor;
        Console.WriteLine(this.GetType().Name +
                          " " +
                          this.Name +
                          " a été notifié par "
                          + sujet.GetType().Name +
                          " Val obs: " + sujet.getVal());
        Console.ForegroundColor = old;
    }
}