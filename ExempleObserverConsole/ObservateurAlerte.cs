using ExempleObserverConsole.observable;

namespace ExempleObserverConsole;

public class ObservateurAlerte: IObservable
{
    private ConsoleColor ForegroundColor { get; set; }

    public string Name { get; }

    public ObservateurAlerte(string name, ConsoleColor? foregroundColor = ConsoleColor.White)
    {
        this.Name = name;
        if (foregroundColor != null) ForegroundColor = (ConsoleColor) foregroundColor;
    }

    public void Actualiser(ISujet sujet)
    {
        int val = sujet.getVal();
        if (val == 15)
        {
            Console.WriteLine("Nous somme en 19! PREPAREZ-VOUS!");
            return;
        }
        
        if (val > 20)
        {
            Console.WriteLine("ALERT!!");
        }
        else if (val > 10)
        {
            Console.WriteLine("\nAttention!");
        }
        Console.ResetColor();
    }
    
}