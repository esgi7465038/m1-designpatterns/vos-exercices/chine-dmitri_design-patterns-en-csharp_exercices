namespace ExempleObserverConsole.observable;

public interface IObservable
{
    void Actualiser(ISujet sujet);
}