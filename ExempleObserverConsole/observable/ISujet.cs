namespace ExempleObserverConsole.observable;

public interface ISujet
{
    public void AjouteObservateur(IObservable observateur);

    public void RetireObservateur(IObservable observateur);

    public void NotifierObservateurs();

    public int getVal();
}